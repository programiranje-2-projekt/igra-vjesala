#ifndef MENUHEADER_H
#define MENUHEADER_H
#include "Korisnikheader.h"

void menu();
void printMenu();
void zapisiUDatoteku(IGRAC* igraci, int brojacIgraca);
IGRAC* citajIzDatoteke(char* polje);
void pretrazivanjeIgraca();

#endif // MENUHEADER_H

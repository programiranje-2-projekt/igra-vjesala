#include "Menuheader.h"
#include "Standardheader.h"
#include "Korisnikheader.h"


void printMenu() {
	printf("1. Start\n");
	printf("2. Ljestvica sortirana po bodovima\n");
	printf("3. Ljestvica sortirana po imenima\n");
	printf("4. Pretrazivanje igraca\n");
	printf("5. Zapisi u datoteku\n");
	printf("6. Izlaz\n");
	printf("\n");
	printf("Izaberite opciju: ");

}
void menu(){
	int inputOption;
	IGRAC* igraci;
	int brojacIgraca;

	do {
		printMenu();

		scanf("%d", &inputOption); getchar();

		switch (inputOption) {

		case 1:
			
			break;
		case 2:
			
			break;
		case 3:

			break;
		case 4:
			pretrazivanjeIgraca();
			break;
		case 5:
			zapisiUDatoteku(igraci, brojacIgraca);
			break;
		case 6:
			exit(EXIT_SUCCESS);
			break;
		default:
			puts("Ta operacija ne postoji!\n");
			break;
		}

	} while (inputOption != 3);

}

void zapisiUDatoteku(IGRAC* igraci, int brojacIgraca) {

	FILE* ljestvica = fopen("ljestvica.bin", "wb");
	if (ljestvica == NULL) {
		perror("Nije se mogla otvoriti datoteka!");
		exit(-1);
	}
	fwrite(&brojacIgraca, sizeof(int), 1, ljestvica);
	fwrite(igraci, sizeof(IGRAC), brojacIgraca, ljestvica);
	

	fclose(ljestvica);

}

IGRAC* citajIzDatoteke(char* polje) {

	FILE* ljestvica = fopen(polje, "rb");
	if (ljestvica == NULL) {
		perror("Nije se mogla otvoriti datoteka!");
		exit(-1);
	}
	int brojacIgraca;
	fread(&brojacIgraca, sizeof(int), 1, ljestvica);
	IGRAC* igraci = (IGRAC*)malloc(brojacIgraca * sizeof(IGRAC));
	fread(igraci, sizeof(IGRAC), brojacIgraca, ljestvica);


	fclose(ljestvica);
	return igraci;
}

IGRAC* pretrazivanjeIgracaPoBodovima(int bodovi) {
	IGRAC* igraci = citajIzDatoteke("ljestvica.bin");

	int i;
	int brojacIgraca = citajBrojIzDatoteke("ljestvica.bin");
	for (i = 0; i < brojacIgraca; i++) {
		if (igraci[i].bodovi == bodovi) {
			return &igraci[i];
		}
	}
	return NULL;

}

IGRAC* pretrazivanjeIgracaPoImenu(char* imeIgraca) {
	IGRAC* igraci = citajIzDatoteke("ljestvica.bin");

	int i;
	int brojacIgraca = citajBrojIzDatoteke("ljestvica.bin");
	for (i = 0; i < brojacIgraca; i++) {
		if (strcmp(igraci[i].ime, imeIgraca) == 0) {
			return &igraci[i];
		}
	}
	return NULL;

}

int citajBrojIzDatoteke(char* polje) {
	FILE* ljestvica = fopen(polje, "rb");
	if (ljestvica == NULL) {
		perror("Nije se mogla otvoriti datoteka!");
		exit(-1);
	}
	int brojacIgraca;
	fread(&brojacIgraca, sizeof(int), 1, ljestvica);
	fclose(ljestvica);
	return brojacIgraca;
}